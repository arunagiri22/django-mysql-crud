from django.db import models

# Create your models here.
class Student(models.Model):
	reg_no = models.CharField(max_length=50)
	name = models.CharField(max_length=100)
	mobile = models.CharField(max_length=10)
	email = models.EmailField(max_length=100)
	school = models.CharField(max_length=100)
	class Meta:
		db_table = 'student_master'